<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin users router
Route::prefix('admin')->middleware('checkrole')->group(function () {
    Route::get('/', 'back\AdminController@index')->name('admin.index');
    Route::get('/users', 'back\UserController@index')->name('admin.users');
    Route::get('/user/{user}', 'back\UserController@show')->name('admin.user.show');
    Route::get('/user/edit-status/{user}', 'back\UserController@editstatus')->name('admin.user.editStatus');
    Route::get('/user/delete/{user}', 'back\UserController@destroy')->name('admin.user.delete');
});
//admin categories router
Route::prefix('admin/categories')->middleware('checkrole')->group(function () {
    Route::get('/', 'back\CategoryController@index')->name('admin.categories');
    Route::post('/store', 'back\CategoryController@store')->name('admin.category.store');
    Route::get('/edit/{category}', 'back\CategoryController@edit')->name('admin.category.edit');
    Route::put('/update/{category}', 'back\CategoryController@update')->name('admin.category.update');
    Route::get('/delete/{category}', 'back\CategoryController@destroy')->name('admin.category.delete');
});

//admin comments router
Route::prefix('admin/comments')->middleware('checkrole')->group(function () {
    Route::get('/', 'back\CommentController@index')->name('admin.comments');
    Route::get('/show/{comment}', 'back\CommentController@show')->name('admin.comment.show');
    Route::get('/edit-status/{comment}', 'back\CommentController@editstatus')->name('admin.comment.editStatus');
    Route::get('/delete/{category}', 'back\CommentController@destroy')->name('admin.comment.delete');
});

//admin articles router
Route::prefix('admin/articles')->middleware('checkrole')->group(function () {
    Route::get('/', 'back\ArticleController@index')->name('admin.articles');
    Route::get('/create', 'back\ArticleController@create')->name('admin.article.create');
    Route::post('/store', 'back\ArticleController@store')->name('admin.article.store');
    Route::get('/edit/{article}', 'back\ArticleController@edit')->name('admin.article.edit');
    Route::put('/update/{article}', 'back\ArticleController@update')->name('admin.article.update');
    Route::get('/delete/{article}', 'back\ArticleController@destroy')->name('admin.article.delete');
});


//user profile routers
Route::prefix('/profile')->middleware('auth', 'checkuser')->group(function () {
    Route::get('/{user}', 'UserController@index')->name('profile');
    Route::get('/edit/{editType}/{user}', 'UserController@edit')->name('profile.edit');
    Route::put('/update/{user}', 'UserController@update')->name('profile.update');
});



Auth::routes();
Route::get('/', 'indexController@index')->name('index');
Route::get('/article/{article}', 'front\ArticleController@show')->name('article');
Route::post('/comment/store/{article}', 'front\CommentController@store')->name('comment.store');
Route::get('/articles/category/{category}', 'front\FiltrationController@category')->name('category.filter');
Route::get('/articles/search/{txet}', 'front\FiltrationController@search')->name('category.search');

