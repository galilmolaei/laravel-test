


function getRandomColor() {
    var letters = '456789AB';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 8)];
    }
    return color;
}

var color1  = getRandomColor();
var color2  = getRandomColor();
var color3  = getRandomColor();
var color4  = getRandomColor();
var color5  = getRandomColor();
var color6  = getRandomColor();
var color7  = getRandomColor();
var opacity = '55';
var ctx = document.getElementById('myChart').getContext('2d');
Chart.defaults.global.defaultFontColor='white';


var days = Array();
const changeDate = (date) => {
    let dateArray = date.split('/');
    for (let i = 1; i < 7; i++) {
        let day = dateArray[2];
        let mounth = dateArray[1];
        let year = dateArray[0];
        days[i-1] = year+'/'+mounth+'/'+(parseInt(day)-i);
    }
}
const largestValue = (array) => {
    let newArray = [parseInt(array[8]), parseInt(array[7]), parseInt(array[6]), parseInt(array[5]), parseInt(array[4]), parseInt(array[1]), parseInt(array[0])];
    var min = Math.max.apply(Math, newArray)
    return min;
}
const smallestValue = (array) => {
    let newArray = [parseInt(array[8]), parseInt(array[7]), parseInt(array[6]), parseInt(array[5]), parseInt(array[4]), parseInt(array[1]), parseInt(array[0])];
    var min = Math.min.apply(Math, newArray)
    return min;
}
var visiteArray;
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        visiteArray =  xhttp.responseText.split('|');
        changeDate(visiteArray[3])
        document.querySelector('.all-visite').innerHTML = visiteArray[2]+' بار ';
        document.querySelector('.today-visite').innerHTML = visiteArray[0]+' بار ';
        var myChart = new Chart(ctx, {
            //bar line doughnut
            type: 'line',
            data: {
                labels: [days[5], days[4], days[3], days[2], days[1], days[0], visiteArray[3]],
                datasets: [{
                    label: 'آمار بازدید سایت',
                    data: [visiteArray[8], visiteArray[7], visiteArray[6], visiteArray[5], visiteArray[4], visiteArray[1], visiteArray[0]],
                    backgroundColor: [
                        color1+'88',
                    ],
                    borderColor: [
                        color1,
                        color2,
                        color3,
                        color4,
                        color5,
                        color6,
                        color6,
                    ],
                    borderWidth: 1,
                    scaleFontColor: "#FFFFFF"
                }]
            },
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: 'آمار بازدید سایت',
                    position: 'top',
                    fontSize: 16,
                    padding: 20,
                    fontColor: 'white',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            max: largestValue(visiteArray) + smallestValue(visiteArray),
                            beginAtZero: true
                        }
                    }]
                }

            }
        });
        var ctx2 = document.getElementById('myChart-2').getContext('2d');
        var myChart = new Chart(ctx2, {
            //bar line doughnut
            type: 'bar',
            data: {
                labels: [days[5], days[4], days[3], days[2], days[1], days[0], visiteArray[3]],
                datasets: [{
                    labels: 'آمار کاربران جدید',
                    data: [
                        42,
                        124,
                        23,
                        124,
                        34,
                        22,
                        54,

                    ],
                    backgroundColor: [
                        color1+'88',
                        color2+'88',
                        color3+'88',
                        color4+'88',
                        color5+'88',
                        color6+'88',
                    ],
                    borderColor: [
                        color1+opacity,
                        color2+opacity,
                        color3+opacity,
                        color4+opacity,
                        color5+opacity,
                        color6+opacity,
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: 'آمار کاربران',
                    position: 'top',
                    fontSize: 16,
                    padding: 20,
                    fontColor: 'white',
                },

            }
        });
    }
};
xhttp.open("get", "back/structures/visit-stats.txt");
xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xhttp.send();


var xhttpo = new XMLHttpRequest();
var color1  = getRandomColor();
var color2  = getRandomColor();
var color3  = getRandomColor();
var color4  = getRandomColor();
var color5  = getRandomColor();
var color6  = getRandomColor();
var color7  = getRandomColor();
var opacity = '55';



