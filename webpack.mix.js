const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
        'resources/views/front/css/main.css',
        'resources/views/front/css/post.css',
        'resources/views/front/css/filter.css',
        'resources/views/front/css/all.css',
    ],
    'public/front/css/public-pages.css');

mix.styles([
        'resources/views/front/js/main.js',
        'resources/views/front/js/post.js',
    ],
    'public/front/js/public-pages.js');
