        <div class="sidebar">
            <div class="aside" onclick="openSidebar(this)">
                <div class="aside-container">
                    <a href="{{ route('admin.index') }}" class="sidebar-elem" onclick="openSideMenu(this)">
                        <div class="menu-title">
                            <div class="menu-name">
                                <i class="material-icons">subtitles</i>
                                داشبورد
                            </div>
                        </div>
                    </a>
                    <div class="sidebar-elem" onclick="openSideMenu(this)">
                        <div class="menu-title">
                            <div class="menu-name">
                                <i class="material-icons">people</i>
                                کاربران
                            </div>
                            <i class="material-icons arrow-icon">keyboard_arrow_right</i>
                        </div>
                        <div class="drop-down-aside-menu">
                            <a href="{{ route('admin.users') }}" class="menu-name">
                                <i class="material-icons">people</i>
                                لیست کاربران
                            </a>
                        </div>
                    </div>
                    <div class="sidebar-elem" onclick="openSideMenu(this)"> 
                        <div class="menu-title">
                            <div class="menu-name">                                                                 
                                <i class="material-icons">forum</i>
                                گفتگوها
                            </div>
                            <i class="material-icons arrow-icon">keyboard_arrow_right</i>                                           
                        </div>
                        <div class="drop-down-aside-menu">
                            <a href="{{ route('admin.comments') }}" class="menu-name">
                                <i class="material-icons">comment</i>
                                کامنت ها
                            </a>
                        </div>
                    </div>
                    <div class="sidebar-elem" onclick="openSideMenu(this)"> 
                        <div class="menu-title">
                            <div class="menu-name">                                                                 
                                <i class="material-icons">forum</i>
                                دسته بندی
                            </div>
                            <i class="material-icons arrow-icon">keyboard_arrow_right</i>                                           
                        </div>
                        <div class="drop-down-aside-menu">
                            <a href="{{ route('admin.categories') }}" class="menu-name">
                                <i class="material-icons">comment</i>
                                دسته بندی ها
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>