<div class="loading" style="display: flex; justify-content: center;align-items: center; width: 100vw; height: 100vh; position: fixed; background-color: #005a5a; z-index: 100; top: 0; right: 0">
    <img style="height: 5vw;" src="{{url('back/img/loading-gif.gif')}}"/>
</div>
<div class="header">
        <div class="navigation">
            <div class="nav">
                <div class="nav-right">
                    <div class="page-title" onclick="openSidebar(this)">
                        <i class="material-icons">dashboard</i>
                        <span class="page-name">dashboard</span>
                    </div>
                </div>
                <div class="humburgur">
                    <i class="material-icons" onclick="openHumburgurMenu()">menu</i>
                </div>
                <div class="nav-left">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="exit">
                        <i class="material-icons">exit_to_app</i>
                        خروج از حساب
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <a href="{{ route('index') }}" class="show-website">
                        <i class="material-icons">web</i>
                        مشاهده وبسایت
                    </a>
                    <div href="#" class="admin-avatar">
                        <i class="material-icons">account_circle</i>
                        {{ Auth::user()->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>
