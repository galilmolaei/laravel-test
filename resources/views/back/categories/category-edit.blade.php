@extends('back.main')

@section('title')
    ویرایش دسته بندی
@endsection

@section('content')
    <div class="section">
        <div class="menu-list-day">
            <form id="form_2" class="form-data" action="{{route('admin.category.update', $category->id)}}" method="post">
                @csrf
                @method('put')
                <div class="day-date">ویرایش دسته بندی</div>
                <div class="input-data">
                    <label for="food-name">نام : </label>
                    <input autofocus placeholder="نام" type="text" name="name" class="input" id="food-name" value="{{$category->name}}">
                </div>
                @error('name') {{$message}} @enderror
                <div class="input-data">
                    <label for="food-credit">شناسه : </label>
                    <input placeholder="شناسه" type="text" name="slug" class="input" id="food-price" value="{{$category->slug}}">
                </div>
                @error('slug') {{$message}} @enderror
                <div id="array-list"></div>
                <div class="submit">
                    <label for="submit-u" class="submit"><span>افزودن</span></label>
                    <input type="submit" id="submit-u" style="display: none">
                </div>
            </form>
        </div>
    </div>
@endsection
