@extends('back.main')

@section('title')
    دسته بندی ها
@endsection

@section('content')
    <div class="section">
        <div class="menu-list-day">
            <form id="form_2" class="form-data" action="{{route('admin.category.store')}}" method="post">
                @csrf
                <div class="day-date">افزودن دسته بندی</div>
                <div class="input-data">
                    <label for="food-name">نام : </label>
                    <input autofocus placeholder="نام" type="text" name="name" class="input" value="{{old('name')}}"
                           id="food-name">
                </div>
                @error('name') {{$message}} @enderror
                <div class="input-data">
                    <label for="food-credit">شناسه : </label>
                    <input placeholder="شناسه" type="text" name="slug" class="input" value="{{old('slug')}}"
                           id="food-price">
                </div>
                @error('slug') {{$message}} @enderror
                <div id="array-list"></div>
                <div class="submit">
                    <label for="submit-u" class="submit"><span>افزودن</span></label>
                    <input type="submit" id="submit-u" style="display: none">
                </div>
            </form>
        </div>
        <div class="menu-list-day">
            <div class="day-date"> دسته بندی ها</div>
            <div class="table">
                <table>
                    <tr>
                        <th>نام</th>
                        <th>شناسه</th>
                        <th>مشاهده</th>
                        <th>حذف</th>
                    </tr>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->name}}</td>
                            <td>{{$category->slug}}</td>
                            <td><a href="{{route('admin.category.edit', $category->id)}}">ویرایش</a></td>
                            <td>
                                <a onclick="return confirm('ایا میخواهید @php echo $category->name @endphp را حذف کنید؟')"
                                   href="{{route('admin.category.delete', $category->id)}}">حذف</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$categories->links()}}
        </div>
    </div>
@endsection
