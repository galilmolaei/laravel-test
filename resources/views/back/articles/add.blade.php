@extends('back.main')

@section('title')
    اضافه کردن پست
@endsection

@section('content')
    <div class="section">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
        <form class="form-data" action="{{route('admin.article.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="input-data">
                <label for="name"> عنوان پست : </label>
                <input autofocus placeholder="عنوان پست" type="text" name="name" class="input" id="name" value="{{ old('name') }}">
            </div>
            <div class="input-data">
                <label for="slug">نام مستعار پست : </label>
                <input placeholder="نام مستعار پست" type="text" name="slug" class="input" id="slug" value="{{ old('slug') }}">
            </div>
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <div class="input-data">
                <label for="img">توضیحات مطلب</label>
                <div id="writer">
                    <ul id="container-ul">
                        <select onclick=" heading(this)" >
                            <option>heading</option>
                            <option value="H1">h1</option>
                            <option value="H2">h2</option>
                            <option value="H3">h3</option>
                            <option value="H4">h4</option>
                            <option value="H5">h5</option>
                            <option value="H6">h6</option>
                        </select>
                        <button  type="button" onclick="paragraph()" >پاراگراف</button>
                        <button type="button" onclick="blockQoute()" class="material-icons md-18" >format_quote</button>
                        <button type="button" onclick="bold()" ><b> B</b></button>
                        <button type="button" onclick="underLine()" ><u>u</u></button>
                        <button type="button" onclick="italic()" ><i>i</i></button>
                        <button type="button" onclick="li()" class="material-icons md-18" >format_list_bulleted</button>
                        <button type="button" onclick="justifyRight()" class="material-icons md-18" >format_align_right</button>
                        <button type="button" onclick="justifyCenter()" class="material-icons md-18" >format_align_center</button>
                        <button type="button" onclick="justifyLeft()" class="material-icons md-18">format_align_left</button>
                    </ul>
                    </ul>
                    </ul>
                    <textarea style="display: none" id="post-text" name="description">{{ old('description') }}</textarea><br>
                    <div data-text="Enter comment...." contenteditable="true" id="output">{!! old('description') !!}</div>
                </div>
            </div>
            <div class="input-data">
                <label for="">وضعیت پست : </label>
                <div class="meal-select custome-select" style="width: 83%">
                    <select name="status">
                        <option value="0">در دست انتشار</option>
                        <option value="0">در دست انتشار</option>
                        <option value="1">منتشر شده</option>
                    </select>
                </div>
            </div>
            <div class="input-data">
                <label for="">دسته بندی : </label>
                <div class="meal-select custome-select" style="width: 83%">
                    <select name="category">
                        <option value="">انتخاب کنید ...</option>
                        @foreach($categories as $cat_id => $cat_name)
                            <option value="{{$cat_id}}">{{$cat_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-data file">
                <label for="img">انتخاب عکس پست</label>
                <label for="img">انتخاب عکس پست</label>
                <input type="file" name="img" value="/hello" id="img">
            </div>
            {{--<div class="input-data">
                <label for="email">توضیحات عکس : </label>
                <input placeholder="نام جایگزین عکس (alt)" type="text" name="img-alt" class="input" id="email">
            </div>--}}
            <div class="submit">
                <label for="submit" class="submit"><span>تایید</span></label>
                <input type="submit" value="edit-food" id="submit">
            </div>
        </form>
        <div class="custome-style">
            <ul>
                <li>متن جایگزین عکس به موتورهای جستجو کمک میکند تا صفحات سایت شما و عکسهای آنها را بهتر ایندکس کنند </li>
            </ul>
        </div>
    </div>
@endsection
