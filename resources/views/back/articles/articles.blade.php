@extends('back.main')

@section('title')
    مطالب
@endsection

@section('content')
    <div class="section">
        <div class="menu-list-day">
            <div class="day-date">مطالب</div>
            <div class="table">
                <table>
                    <tr>
                        <th>نام</th>
                        <th>شناسه</th>
                        <th>نویسنده</th>
                        <th>دسته بندی</th>
                        <th>ویرایش</th>
                        <th>حذف</th>
                    </tr>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{$article->name}}</td>
                            <td>{{$article->slug}}</td>
                            <td>{{$article->user->name}}</td>
                            <td>@foreach($article->categories as $category) {{$category->name}} @endforeach </td>
                            <td><a href="{{route('admin.article.edit', $article->id)}}">ویرایش</a></td>
                            <td>
                                <a onclick="return confirm('ایا میخواهید @php echo $article->name @endphp را حذف کنید؟')"
                                   href="{{route('admin.article.delete', $article->id)}}">حذف</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$articles->links()}}
        </div>
    </div>
@endsection


