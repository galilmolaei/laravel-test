@extends('back.main')

@section('title')
    مشاهده نظر
@endsection

@section('content')
<div class="section">
        <div class="menu-list-day">
            <div class="day-date">مشاهده نظر</div>
            <div class="table">
                <table>
                    <tr>
                        <th>نام</th>
                        <th>تاریخ</th>
                        <th>وضعیت</th>
                        <th>حذف</th>
                    </tr>
                    <tr>
                            @switch($comment->status)
                            @case(0)
                            @php
                                $url = route('admin.comment.editStatus', $comment->id);
                                $status = '<a href="'.$url.'" class="bgc-red">منتشر نشده</a>' @endphp
                            @break
                            @case(1)
                            @php
                                $url = route('admin.comment.editStatus', $comment->id);
                                $status = '<a href="'.$url.'" class="bgc-green">منتشر شده</a>' @endphp
                            @break
                        @endswitch
                        <td>{{ $comment->name }}</td>
                        <td>{{ jdate($comment->created_at)->format('%d/%m/%Y') }}</td>
                        <td>{!!$status!!}</td>
                        {{-- <td>@foreach($article->categories as $category) {{$category->name}} @endforeach </td> --}}
                        <td>
                            <a onclick="return confirm('ایا میخواهید @php echo $comment->name @endphp را حذف کنید؟')" href="{{route('admin.comment.delete', $comment->id)}}">حذف</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="menu-list-day">
            <div class="day-date">متن کامنت</div>
            <div class="content">
                {{ $comment->content }}
            </div>
        </div>
    </div>
@endsection


