@extends('back.main')

@section('title')
    نظرات
@endsection

@section('content')
    <div class="section">
        <div class="menu-list-day">
            <div class="day-date">نظرات</div>
            <div class="table">
                <table>
                    <tr>
                        <th>خلاصه نظر</th>
                        <th>نویسنده</th>
                        <th>تاریخ</th>
                        <th>وضعیت</th>
                        <th>برای پست</th>
                        <th>مشاهده</th>
                        <th>حذف</th>
                    </tr>
                    @foreach($comments as $comment)
                    
                    @switch($comment->status)
                    @case(0)
                    @php
                        $url = route('admin.comment.editStatus', $comment->id);
                        $status = '<a href="'.$url.'" class="bgc-red">منتشر نشده</a>' @endphp
                    @break
                    @case(1)
                    @php
                        $url = route('admin.comment.editStatus', $comment->id);
                        $status = '<a href="'.$url.'" class="bgc-green">منتشر شده</a>' @endphp
                    @break
                @endswitch
                        <tr>
                            <td>{{substr($comment->content,0,40)}}</td>
                            <td>{{$comment->name}}</td>
                            <td>{{ jdate($comment->created_at)->format('%d/%m/%Y') }}</td>
                            <td>{!!$status!!}</td>
                            <td>{{ $comment->article->name }}</td>
                            {{-- <td>@foreach($article->categories as $category) {{$category->name}} @endforeach </td> --}}
                            <td><a href="{{route('admin.comment.show', $comment->id)}}">مشاهده</a></td>
                            <td>
                                <a onclick="return confirm('ایا میخواهید @php echo $comment->name @endphp را حذف کنید؟')" href="{{route('admin.comment.delete', $comment->id)}}">حذف</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$comments->links()}}
        </div>
    </div>
@endsection


