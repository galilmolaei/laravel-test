@extends('back.main')

@section('title')
    کاربران
@endsection

@section('content')
    <div class="section">
        <div class="menu-list-day">
            <div class="day-date">مشخصات کاربران</div>
            <div class="table">
                <table>
                    <tr>
                        <th>نام</th>
                        <th>شماره تلفن</th>
                        <th>وضعیت</th>
                        <th>نقش</th>
                        <th>مشاهده</th>
                        <th>حذف</th>
                    </tr>
                    @foreach($users as $user)
                        @switch($user->role)
                            @case(0)
                            @php $role = 'کاربر' @endphp
                            @break
                            @case(1)
                            @php $role = 'مدیر' @endphp
                            @break
                        @endswitch
                        @switch($user->status)
                            @case(0)
                            @php
                                $url = route('admin.user.editStatus', $user->id);
                                $status = '<a href="'.$url.'" class="bgc-red">بلاک</a>' @endphp
                            @break
                            @case(1)
                            @php
                                $url = route('admin.user.editStatus', $user->id);
                                $status = '<a href="'.$url.'" class="bgc-green">فعال</a>' @endphp
                            @break
                        @endswitch
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->phone_number}}</td>
                            <td>{!!$status!!}</td>
                            <td>{{$role}}</td>
                            <td><a href="{{route('admin.user.show', $user->id)}}">مشاهده</a></td>
                            <td><a onclick="return confirm('ایا میخواهید @php echo $user->name @endphp را حذف کنید؟')" href="{{route('admin.user.delete', $user->id)}}">حذف</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$users->links()}}
        </div>
    </div>
@endsection
