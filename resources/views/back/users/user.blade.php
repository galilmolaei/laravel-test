@extends('back.main')

@section('title')
    مشخصات کاربر
@endsection

@section('content')
    <div class="section">
        <div class="menu-list-day">
            <div class="day-date">مشخصات کاربر</div>
            <div class="table">
                <table>
                    <tr>
                        <th>نام</th>
                        <th>شماره تلفن</th>
                        <th>وضعیت</th>
                        <th>نقش</th>
                    </tr>
                    @switch($user->role)
                        @case(0)
                        @php $role = 'کاربر' @endphp
                        @break
                        @case(1)
                        @php $role = 'مدیر' @endphp
                        @break
                    @endswitch
                    @switch($user->status)
                        @case(0)
                        @php
                            $url = route('admin.user.editStatus', $user->id);
                            $status = '<a href="'.$url.'" class="bgc-red">بلاک</a>' @endphp
                        @break
                        @case(1)
                        @php
                            $url = route('admin.user.editStatus', $user->id);
                            $status = '<a href="'.$url.'" class="bgc-green">فعال</a>' @endphp
                        @break
                    @endswitch
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->phone_number}}</td>
                        <td>{!!$status!!}</td>
                        <td>{{$role}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="menu-list-day">
            <div class="day-date">سایر مشخصات</div>
            <div class="table">
                <table>
                    <tr>
                        <th>ایمیل</th>
                        <th>تعداد کامنتها</th>
                        <th>تاریخ عضویت</th>
                    </tr>
                    <tr>
                        <td>{{$user->email}}</td>
                        <td>8</td>
                        <td>{{$user->created_at}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection
