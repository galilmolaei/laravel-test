<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{url('back/styles/styles.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="{{url('back/js/main.js')}}"></script>
</head>
<body>
@include('back.structure.header')
<div class="main">
    @include('back.structure.sidebar')
    @yield('content')
</div>
<script src="{{url('back/js/custome.js')}}"></script>
</body>
</html>
