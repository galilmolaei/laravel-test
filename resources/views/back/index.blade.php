
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>داشبورد | پنل ادمین</title>
    <link rel="stylesheet" href="back/styles/styles.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="back/js/main.js"></script>
</head>
<body class="animating">
    @include('back.structure.header')
    <div class="main">
        @include('back.structure.sidebar')
        <div class="section">
            <div class="section-container">
                <div class="middle">
                    <div class="middle-header">
                        <div class="middle-header-right">
                            <div class="middle-header-item">
                                <div>تعداد بازدیدهای امروز</div>
                                <div class="today-visite">52 بار</div>
                            </div>
                            <div class="middle-header-item">
                                <div>تعداد بازدیدهای کل</div>
                                <div class="all-visite">1025 بار</div>
                            </div>
                        </div>
                        <div class="middle-header-left">
                            <div class="middle-header-item">
                                <div>تعداد کامنتهای امروز</div>
                                <div>10 عدد</div>
                            </div>
                            <div class="middle-header-item">
                                <div>کاربران جدید</div>
                                <div>12 نفر</div>
                            </div>
                        </div>
                    </div>
                    <div class="middle-main">
                        <div class="chart-1">
                            <div class="container">
                                <canvas id="myChart" width="600" height="400"></canvas>
                            </div>
                        </div>
                        <div class="chart-2">
                            <div class="container">
                                <canvas id="myChart-2" width="600" height="400"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{url('/back/js/chart/Chart.bundle.min.js')}}"></script>
    <script src="{{url('/back/js/main.chart.js')}}"></script>





</body>
</html>
</body>

</html>
