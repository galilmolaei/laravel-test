<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    {!! htmlScriptTagJsApi(['lang' => 'fa']) !!}
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
<div class="shadow"></div>
<div class="header">
    <div class="t-nav">
        <div class="navbar-hambor t-hambor" onclick="openNavTMenu()">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="t-nav-main">
            <div class="border-t-nav"></div>
            <a onmouseover="changeBorderpos(this)" class="blue t_nav-element" href="">
                صفحه خانه
            </a>
            <a onmouseover="changeBorderpos(this)" class="blue t_nav-element" href="">
                درباره ما
            </a>
                @if(auth()->check() && Auth()->user()->role == 1)

                <a onmouseover="changeBorderpos(this)" class="blue t_nav-element" href="{{route('admin.index')}}">
                    پنل ادمین
                </a>
                @endif
        </div>
        <dv class="t-nav-left">
            <i class="material-icons">person_outline</i>
            @if(auth::guest())
                <a class="green t_nav-element ml-1 text-white" href="{{route('login')}}">
                    ورود
                </a>
                |
                <a class="green t_nav-element mr-1 text-white" href="{{route('register')}}">
                    ثبت نام
                </a>
            @else
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('خروج') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @endif
        </dv>
    </div>
    <div class="navbar-hambor" onclick="openNavMenu()">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="navbar">
        <div class="navigation">
            <div class="nav">
                <div class="nav-item">
                    <svg class="nav-svg hide-small">
                        <defs>
                            <linearGradient id="grad1">
                                <stop offset="0%" stop-color="rgba(6, 131, 221, 0.267)" />
                                <stop offset="100%" stop-color="#341f97" />
                            </linearGradient>
                        </defs>
                        <rect class="nav-rect" x="5" y="5" rx="17" fill="transparent" stroke="url(#grad1)" width="130"
                              height="30">
                    </svg>
                    <a class="nav-text">لاراول تست</a>
                    <div class="childs-items hide-small">
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                    </div>
                </div>
                <div href="" class="nav-item">
                    <svg class="nav-svg hide-small">
                        <rect class="nav-rect" x="5" y="5" rx="17" fill="transparent" stroke="url(#grad1)" width="130"
                              height="30">
                    </svg>
                    <a href="{{ route('category.filter', 'برنامه-نویسی') }}" class="nav-text">برنامه نویسی</a>
                    <div class="childs-items hide-small">
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                    </div>
                </div>
                <div href="" class="nav-item">
                    <svg class="nav-svg hide-small">
                        <rect class="nav-rect" x="5" y="5" rx="17" fill="transparent" stroke="url(#grad1)" width="130"
                              height="30">
                    </svg>
                    <a href="{{ route('category.filter', 'گرافیک') }}" class="nav-text">گرافیک</a>
                    <div class="childs-items hide-small">
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main">
    @yield('content')
</div>
<script src="{{url('front/js/public-pages.js')}}"></script>
<link rel="stylesheet" href="{{url('front/css/public-pages.css')}}">
@yield('styles')
</body>

</html>
