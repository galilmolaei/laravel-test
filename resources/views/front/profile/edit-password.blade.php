@extends('front.main-index')

@section('content')

    <div class="main">
        <div class="main-profile main-information">

            @include('front.profile.structure.profile-menu')

            <div class="edit-content">
                <form method="post" action="{{route('profile.update', $user->id)}}" class="edit-profile">
                    @csrf
                    @method('put')
                    <input type="password" name="password" id="" placeholder="رمز عبور جدید">
                    @error('password')
                    <div class='err options-profile op-m0'>{{$message}}</div>
                    @enderror
                    <input type="password" name="password_confirmation" id="" placeholder="تکرار رمز عبور">
                    <input type="submit" name="update-password" value="تایید">
                </form>
            </div>
        </div>
    </div>


    <link rel="stylesheet" href="{{url('front/profile/css/profile.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
@endsection
