<div class="profile-menu">
    <div class="header-menu">
        <div class="avatar-img"><img src="../img/maleprofile.png" alt=""></div>
    </div>
    <div class="menu-items">
        <a href="{{route('profile', Auth()->user()->id)}}" class="options-profile">صفحه اصلی</a>
        <a href="{{route('profile.edit', ['information', Auth()->user()->id])}}" class="options-profile">ویرایش اطلاعات</a>
        <a href="{{route('profile.edit', ['password', Auth()->user()->id])}}" class="options-profile">ویرایش رمز عبور</a>
        @if (session('suc'))
            <div class='suc options-profile'>{{session('suc')}}</div>
        @endif
        @if (session('err'))
            <div class='err options-profile'>{{session('err')}}</div>
        @endif
    </div>
</div>
