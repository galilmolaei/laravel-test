<div class="header">
    <div class="nav">
        <div class="navbar">
            <div class="hidden-larg bymargin-side-left">
                <i id='left-menu' class="material-icons in-nav-i" onclick="openLeftMenu()">more_vert</i>
                <div class="sub-left-menu in-nav-dropdown">
                    <a href=""><i class="material-icons hidden-larg">help</i> سوالات متداول</a>
                    <a href=""><i class="material-icons hidden-larg">favorite_border</i> حمایت از ...</a>
                </div>
            </div>
            <div class="right"><a onclick="exit()"><i class="material-icons">exit_to_app</i> &nbsp خروج از حساب کاربری </a></div>
            <div class="left">
                <div class="login-samll hidden-larg"><a href="login.php"><i class="material-icons">person_outline</i> &nbsp ورود | ثبت نام </a></div>
                <div class="left-items"><a href="../about.php"><i class="material-icons">speaker_notes</i> درباره ما</a></div>
                <div class="left-items"><a href="../contact-us.php"><i class="material-icons">forum</i> تماس با ما</a></div>
                <!--<div class="left-items"><a href=""><i class="material-icons">add_circle_outline</i> ثبت خیریه</a></div>-->
                <div class="left-items"><a href=""><i class="material-icons">report</i> ثبت شکایات</a></div>
                <div class="left-items"><a href=""><i class="material-icons">live_help</i> شرایط و ضوابط</a></div>
                <div class="left-items hidden-small">
                    <i id='left-menu' class="material-icons" onclick="openLeftMenu()">more_vert</i>
                    <div class="sub-left-menu sub-left-dropdown">
                        <a href=""><i class="material-icons hidden-larg">help</i> سوالات متداول</a>
                        <a href=""><i class="material-icons hidden-larg">favorite_border</i> حمایت از ...</a>
                    </div>
                </div>
            </div>
            <div class="center"><a href="../"><i class="material-icons">fingerprint</i></a></div>
            <div onclick="openMenu()" class="humborger hidden-larg bymargin-side-right">
                <i class="material-icons">menu</i>
            </div>

            <div class="shadow hidden-larg"></div>
        </div>
    </div>
</div>