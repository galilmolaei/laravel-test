@extends('front.main-index')

@section('content')

    <div class="main">
        <div class="main-profile main-information">

            @include('front.profile.structure.profile-menu')

            <div class="edit-content">
                <form method="post" action="{{route('profile.update', $user->id)}}" class="edit-profile">
                    @csrf
                    @method('put')
                    <input type="text" name="name" id="" placeholder="نام" value="{{$user->name}}">
                    @error('name')
                    <div class='err options-profile op-m0'>{{$message}}</div>
                    @enderror
                    <input type="email" name="email" id="" placeholder="ایمیل" value="{{$user->email}}">
                    @error('email')
                    <div class='err options-profile op-m0'>{{$message}}</div>
                    @enderror
                    <input type="text" name="phone_number" id="" placeholder="ایمیل" value="{{$user->phone_number}}">
                    @error('phone_number')
                    <div class='err options-profile op-m0'>{{$message}}</div>
                    @enderror
                    <input name="update-information" type="submit" value="تایید">
                </form>
            </div>
        </div>
    </div>


    <link rel="stylesheet" href="{{url('front/profile/css/profile.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
@endsection
