@extends('front.main-index')

@section('content')

    <div class="main">
        <div class="main-profile">

            @include('front.profile.structure.profile-menu')

            <div class="profile-content">
                <div class="main-content-profile">
                    <div class="number-help"><span class="highlight">نام کاربری</span> &nbsp&nbsp : &nbsp&nbsp<span>{{$user->name}}</span> </div>
                    <div class="number-help"><span class="highlight">ایمیل</span> &nbsp&nbsp : &nbsp&nbsp<span>{{$user->email}}</span> </div>
                    <div class="order-help"><span class="highlight">تعداد فعالیتهای شما</span>&nbsp&nbsp : &nbsp&nbsp<span>12</span> </div>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="{{url('front/profile/css/profile.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    @endsection
