@extends('front.main-index')

@section('content')
<div class="main">
    <h1 style="display: flex; justify-content: center; width: 100%; color: #787878">{{ $article->name }}</h1>
        <div class="main-post">
            <div class="main-sidebar">
                <div class="aside-container aside-container-max_width">
                    <div class="aside">
                        <div class="aside-header"></div>
                    </div>
                </div>
            </div>
            <div class="main-content">
                <div class="content-container">
                    <div class="post_slice-container">
                        <div class="post_slice">
                            {!! html_entity_decode($article->description) !!}
                        </div>
                    </div>
                </div>
                <div class="comments">
                    @foreach ($comments as $comment)
                    <div class="comment-container">
                            <div class="comment_">
                                <div class="comment">
                                    <div class="comment-avatar">
                                        <div class="comment-avatar-container">
                                            <div class="comment-img">
    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment-name">{{ $comment->name }}</div>
                                    <div class="comment-text">{{ $comment->content }}</div>
                                    <div class="action_to-comment">
                                        <div class="action_to-comment-container">
                                            <span>پاسخ</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="comment_ comment-ansver">
                                <div class="comment">
                                    <div class="comment-avatar">
                                        <div class="comment-avatar-container">
                                            <div class="comment-img">
    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment-name">چنین گفت شاپور</div>
                                    <div class="comment-text">باسلام و عرض احترام اقای موسوی شما درباره پردازش ویدیو با ffmpeg تجربه کاری دارین میتونین راهنماییم کنین.من با ffmpeg در php و لاراول میخوام پردازش ویدیو انجام بدم میخوام به ویدیو یک لوگو با طول و عرض ثابت ی</div>
                                    <div class="action_to-comment">
                                        <div class="action_to-comment-container">
                                            <span>پاسخ</span>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            
                        </div>
                    @endforeach
                    @if (session('suc'))
                        <div style="padding: 5px 10px; border: 1px solid green; border-radius: 10px; margin: 5px"> {{ session('suc') }} </div>
                    @endif
                    <div class="add-comment" >
                            <form method='post' action='{{ route('comment.store', $article->id) }}' class="add-comment-form">
                                @csrf
                                
                                
                                {!! htmlFormSnippet() !!}
                                @if ($errors->any())
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>
                                            {{$error}}
                                        </li>
                                        @endforeach
                                    </ul>
                                @endif
                                @auth
                                <div class="comment-inputs">
                                        <div class="comment-name-container">
                                            <input style="background-color: #fff" disabled value="{{ Auth::user()->name }}" type="text" class="comment-name-input" placeholder='نام کاربری : '>
                                            <input value="{{ Auth::user()->name }}" name="name" type="hidden">
                                        </div>
                                        @error('name')
                                            <div style="padding: 5px 10px; border: 1px solid red; border-radius: 10px; margin: 5px"> {{ $message }} </div>
                                        @enderror
                                        <div class="comment-email-container">
                                            <input style="background-color: #fff" disabled value="{{ Auth::user()->email }}" type="email" class="comment-email-input" placeholder='ایمیل : '>
                                            <input value="{{ Auth::user()->email }}" name="email" type="hidden">
                                        </div>
                                        @error('email')
                                                <div style="padding: 5px 10px; border: 1px solid red; border-radius: 10px; margin: 5px"> {{ $message }} </div>
                                            @enderror
                                    </div>
                                    @else
                                    <div class="comment-inputs">
                                            <div class="comment-name-container">
                                                <input value="{{ old('name') }}" name="name" type="text" class="comment-name-input" placeholder='نام کاربری : '>
                                            </div>
                                            @error('name')
                                                <div style="padding: 5px 10px; border: 1px solid red; border-radius: 10px; margin: 5px"> {{ $message }} </div>
                                            @enderror
                                            <div class="comment-email-container">
                                                <input value="{{ old('email') }}" name="email" type="email" class="comment-email-input" placeholder='ایمیل : '>
                                            </div>
                                            @error('email')
                                                    <div style="padding: 5px 10px; border: 1px solid red; border-radius: 10px; margin: 5px"> {{ $message }} </div>
                                                @enderror
                                        </div>
                                @endauth
                                <div class="comment-description-container">
                                    <textarea name="content" name="" id="" cols="30" rows="10" class="comment-description" placeholder="متن نظر . . ."> {{ old('content') }}</textarea>
                                    @error('content')
                                        <div style="padding: 5px 10px; border: 1px solid red; border-radius: 10px; margin: 5px"> {{ $message }} </div>
                                    @enderror
                                </div>
                                <div class="comment-submit_">
                                    <div class="comment-submit-container">
                                        <button type="submit">ارسال</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>

            <div class="aside-container aside-container-min_width">
                <div class="aside">
                    <div class="aside-header"></div>
                </div>
            </div>
        </div>
    </div>
@endsection