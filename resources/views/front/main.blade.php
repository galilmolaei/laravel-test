<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{url('front/css/index.css')}}">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
<div class="shadow"></div>
<div class="header">
    <div class="imgs">
        <img src="{{url('front/img/top-left.svg')}}" alt="" class="top-left">
        <img src="{{url('front/img/bottom-right.svg')}}" alt="" class="top-right">
        <img src="{{url('front/img/buttom-left.svg')}}" alt="" class="buttom-left">
        <img src="{{url('front/img/header-img.svg')}}" alt="" class="header-img">
    </div>
    <div class="header-text">
        <h1>لاراول تست <span class="red">راهنمایی</span> برای یادگیری</h1>
        <p>لاراول تست راهی نوین جهت یادگیری و افزایش اطلاعات و آموزش فناوری</p>
        <p>هدف ما در لاراول تست نشان دادن راه برای یادگیری سریعتر و کاملتر است</p>
    </div>
    <ul class="social_media">
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a></li>
    </ul>
    <div class="searchbar">
        <form class="search">
            <div class="submit-search-container">
                <button type="submit" class="search-button"><i class="fa fa-search" ></i></button>
            </div>
            <input placeholder="جستجو..." type="text" name="" id="" class="search-input">
        </form>
    </div>
    <a href="" class="go_to_filter">
        <p>جستجوی پیشرفته</p>
    </a>
    <div class="t-nav">
        <div class="navbar-hambor t-hambor" onclick="openNavTMenu()">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="t-nav-main">
            <div class="border-arrow-nav"></div>
            <div class="border-t-nav"></div>
            <a onmouseover="changeBorderpos(this)" class="blue t_nav-element" href="">
                صفحه خانه
            </a>
            <a onmouseover="changeBorderpos(this)" class="blue t_nav-element" href="">
                درباره ما
            </a>
            @if(auth()->check() && Auth()->user()->role == 1)

                <a onmouseover="changeBorderpos(this)" class="blue t_nav-element" href="{{route('admin.index')}}">
                    پنل ادمین
                </a>
            @endif
        </div>
        <div class="t-nav-left">
            <i class="material-icons">person_outline</i>
            @auth()
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('خروج') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
                <a class="green t_nav-element" style="color: #fff; margin-left: 3px;" href="{{route('login')}}">
                    ورود
                </a>
                |
                <a class="green t_nav-element" style="color: #fff; margin-right: 3px;" href="{{route('register')}}">
                    قبت نام
                </a>
            @endauth
        </div>
    </div>
    <div class="navbar-hambor" onclick="openNavMenu()">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="navbar">
        <div class="navigation">
            <div class="nav">
                <div class="nav-item">
                    <svg class="nav-svg hide-small">
                        <defs>
                            <linearGradient id="grad1">
                                <stop offset="0%" stop-color="#00a3e9" />
                                <stop offset="100%" stop-color="#00ff66" />
                            </linearGradient>
                        </defs>
                        <rect class="nav-rect" x="5" y="5" rx="17" fill="transparent" stroke="url(#grad1)" width="130" height="30">
                    </svg>
                    <a class="nav-text">لاراول تست</a>
                    <div class="childs-items hide-small">
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                    </div>
                </div>
                <div href="" class="nav-item">
                    <svg class="nav-svg hide-small">
                        <rect class="nav-rect" x="5" y="5" rx="17" fill="transparent" stroke="url(#grad1)" width="130" height="30">
                    </svg>
                    <a href="{{ route('category.filter', 'برنامه-نویسی') }}" class="nav-text">برنامه نویسی</a>
                    <div class="childs-items hide-small">
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                    </div>
                </div>
                <div href="" class="nav-item">
                    <svg class="nav-svg hide-small">
                        <rect class="nav-rect" x="5" y="5" rx="17" fill="transparent" stroke="url(#grad1)" width="130" height="30">
                    </svg>
                    <a href="{{ route('category.filter', 'گرافیک') }}" class="nav-text">گرافیک</a>
                    <div class="childs-items hide-small">
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                        <a href="">
                            لاراول تست ها
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a onclick="scrollToDown()" class="scroll-button-container">
        <div class="scroll_button">
            <div class="scrol_button-circle"></div>
        </div>
    </a>
</div>
<div class="gradient_color"></div>
@yield('content')
<script src="{{url('front/js/index.js')}}"></script>
</body>
</html>
