<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\comment;
use App\Article;

class CommentController extends Controller
{
    public function store(Article $article ,Request $request)
    {
        $messages = [
            'name.required' => 'فیلد نام را وارد نمایید',
            'email.required' => 'فیلد ایمیل را وارد نمایید',
            'content.required' => 'فیلد متن نظر را وارد نمایید',
            'email.email' => 'فرمت ایمیل وارد شده صحیح نمیباشد',
        ];
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'content' => 'required',
            recaptchaFieldName() => recaptchaRuleName()
        ], $messages);
        $article->comments()->create($request->all());
        return redirect()->back()->with('suc', 'کامنت شما با موفقیت ذخیره شد و پس از تایید مدیریت نمایش داده میشود');
    }

}
