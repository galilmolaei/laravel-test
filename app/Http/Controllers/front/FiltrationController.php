<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\frontmodels\Category;

class FiltrationController extends Controller
{
    public function category(Category $category) {
        $articles = $category->articles()->paginate(20);
        return view('front.index', compact('articles'));
    }
}
