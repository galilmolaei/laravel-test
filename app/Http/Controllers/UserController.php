<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frontmodels\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return view('front.profile.index', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($editType, User $user)
    {
        switch ($editType) {
            case 'password' :
                return view('front.profile.edit-password', compact('user'));
                break;
            case 'information' :
                return view('front.profile.edit-information', compact('user'));;
                break;
            default :
                return redirect(route('profile'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return void
     */
    public function update(Request $request, User $user)
    {
        if (isset($request->password)) {
            $data = [
                'password' => 'min:8|same:password_confirmation',
            ];
            $messages = [
                'password.min' => 'رمز وارد شده شامل حداقل طول مورد نیاز نیست',
                'password.same' => 'رمز عبور با تکرار آن مطابقت ندارد',
            ];
            $validateData = $request->validate($data, $messages);
            $password = Hash::make($request->password);
            $user->password = $password;
        } else {
            $data = [
                'name' => 'required|max:40',
                'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
                'phone_number' => 'required|regex:/[0-9]/||max:12|min:10|unique:users,phone_number,' . $user->id,
            ];
            $messages = [
                'name.required' => 'وارد کردن نام کاربری الزامی است',
                'name.max' => 'حداکثر طول نام 50 کاراکتر است',
                'email.required' => 'وارد کردن ایمیل الزامی است',
                'email.string' => 'ایمیل تنها میتواند شامل حروف باشد',
                'email.max' => 'حداکثر طول ایمیل 255 کاراکتر است',
                'email.unique' => 'این ایمیل قبلا ثبت شده است',
                'phone_number.unique' => 'این شماره تلفن قبلا ثبت شده است',
                'phone_number.max' => 'شماره تلفن دارای فرمت صحیح نمیباشد',
                'phone_number.min' => 'شماره تلفن دارای فرمت صحیح نمیباشد',
                'phone_number.regex' => 'شماره تلفن دارای فرمت صحیح نمیباشد',
                'phone_number.required' => 'وارد کردن شماره تلفن الزامی است',
            ];
            $validateData = $request->validate($data, $messages);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
        }
        try {
            $user->save();
        } catch (Exception $exception) {
            return redirect(route('profile.edit-information', $user->id))->with('err', $exception->getCode());
        }
        return redirect()->back()->with('suc', 'بروزرسانی اطلاعات موفقیت آمیز بود');
    }

}
