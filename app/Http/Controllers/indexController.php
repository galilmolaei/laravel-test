<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frontmodels\Article;

class indexController extends Controller
{
    public function index() {
        $articles = Article::orderBy('id', 'DESC')->paginate(10);
        return view('front.index', compact('articles'));
    }
}
