<?php

namespace App\frontmodels;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'name', 'email', 'content'
    ];
    protected $attributes = [
        'status' => 0,
    ];
}
