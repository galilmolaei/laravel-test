-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 22, 2020 at 02:37 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amoozeshrah`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `hit` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_slug_unique` (`slug`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name`, `img`, `slug`, `description`, `user_id`, `hit`, `status`, `created_at`, `updated_at`) VALUES
(20, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-21 19:38:59'),
(21, 'جاوا اسکریپت چیست', 'front/img/post-photo50.svg', 'جاوا-اسکریپت-چیست', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-22 02:41:23'),
(22, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript3', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-21 19:38:59'),
(23, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript4', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-21 19:38:59'),
(24, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript5', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 48, 1, '2020-07-21 19:38:59', '2020-07-22 07:35:25'),
(25, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript6', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-21 19:38:59'),
(26, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript7', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-21 19:38:59'),
(27, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript8', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 2, 1, '2020-07-21 19:38:59', '2020-07-22 03:12:26'),
(28, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript9', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 9, 1, '2020-07-21 19:38:59', '2020-07-22 08:11:27'),
(29, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript10', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-22 00:38:59', '2020-07-21 19:38:59'),
(30, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript11', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 19:38:59', '2020-07-21 19:38:59'),
(31, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript12', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 21:38:59', '2020-07-21 19:38:59'),
(32, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript13', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 1, 1, '2020-07-21 23:38:59', '2020-07-21 19:38:59'),
(33, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript14', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 2, 1, '2020-07-21 19:38:59', '2020-07-22 02:58:57'),
(34, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript15', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 2, 1, '2020-07-21 19:38:59', '2020-07-22 03:01:50'),
(35, 'جاوا اسکریپت', 'front/img/post-photo50.svg', 'javaScript16', '&lt;div&gt;جاوا اسکریپت (JavaScript) یک زبان سطح بالا (HighLevel) است که اولین نسخه آن در سال 1995 معرفی شد. و به همراه (HTML , CSS) یکی از سه عنصر اصلی سازنده صفحات وب است.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;این زبان در ابتدا با نام Mocha در شرکت نت اسکیپ و توسط شخصی به نام برندان ایچ ایجاد شد. سپس این نام به LiveScript و در نهایت به جاوا اسکریپت تغییر کرد.اولین استفاده از این زبان در مرورگر Netscape Navigator و نسخه 2.0B3 بود. در سال 1996 این زبان توسط سازمان اکما استاندارد شد. و نسخه های مختلف آن اکما اسکریپت نام گرفت.&lt;/div&gt;&lt;div&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;در ادامه در مورد تمام این مسائل توضیح داده خواهد شد.&lt;/div&gt;', 16, 4, 1, '2020-07-21 22:38:59', '2020-07-22 03:49:13');

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
CREATE TABLE IF NOT EXISTS `article_category` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article_category`
--

INSERT INTO `article_category` (`id`, `article_id`, `category_id`) VALUES
(4, 15, 2),
(5, 16, 3),
(6, 17, 3),
(7, 18, 3),
(8, 19, 3),
(9, 20, 3),
(10, 21, 3),
(11, 22, 2),
(12, 23, 3),
(13, 24, 2),
(14, 25, 3),
(15, 26, 2),
(16, 27, 3),
(17, 28, 2),
(18, 29, 2),
(19, 30, 2),
(20, 32, 2),
(21, 32, 3);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(2, 'گرافیک', 'گرافیک', '2020-07-15 07:00:00', '2020-07-15 07:00:00'),
(3, 'برنامه نویسی', 'برنامه-نویسی', '2020-07-20 07:08:55', '2020-07-21 05:05:58');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `article_id`, `content`, `name`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 24, 'کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی ', 'dwalves', 'dwalves@gmail.com', 1, '2020-07-21 07:00:00', '2020-07-22 04:26:00'),
(2, 24, 'کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی ', 'dwalves', 'dwalves@gmail.com', 0, '2020-07-21 07:00:00', '2020-07-21 07:00:00'),
(3, 24, 'کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی ', 'dwalves', 'dwalves@gmail.com', 0, '2020-07-21 07:00:00', '2020-07-21 07:00:00'),
(4, 24, 'کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی کامنت تستی ', 'dwalves', 'dwalves@gmail.com', 1, '2020-07-21 07:00:00', '2020-07-22 06:21:38'),
(5, 24, 'سلام خیلی عالی بود', 'dwalves', 'galilmolaei@gmail.com', 0, '2020-07-22 06:07:06', '2020-07-22 06:07:06'),
(6, 24, 'سلام خیلی عالی بود', 'dwalves', 'galilmolaei@gmail.com', 0, '2020-07-22 06:16:18', '2020-07-22 06:16:18'),
(7, 24, 'آموزش خوبی بود', 'dow', 'j@gmail.com', 1, '2020-07-22 06:19:11', '2020-07-22 06:21:41'),
(8, 24, 'مطلب خوبی بود', 'dwalves', 'dwalves@gmail.com', 0, '2020-07-22 07:26:49', '2020-07-22 07:26:49'),
(9, 24, 'مطلب خوبی بود', 'dwalves', 'dwalves@gmail.com', 0, '2020-07-22 07:27:28', '2020-07-22 07:27:28'),
(10, 24, 'مطلب خوبی بود', 'dwalves', 'dwalves@gmail.com', 0, '2020-07-22 07:35:25', '2020-07-22 07:35:25'),
(11, 28, 'پست خوبی بود', 'dwalves', 'dwalves@gmail.com', 0, '2020-07-22 08:11:27', '2020-07-22 08:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_18_000940_change_users_phone_number_column_type', 2),
(5, '2020_07_19_192046_create_categories_table', 3),
(6, '2020_07_20_094643_create_articles_table', 4),
(7, '2020_07_20_101554_create_article_category_table', 4),
(8, '2020_07_21_203823_create_comments_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `phone_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `status`, `phone_number`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 'dwalves', 'dwalves@gmail.com', NULL, '$2y$10$mMDgWrcSumSfZn.FR0pEHOWlGvTJBwkBGacRnLZEl1j9U5I7QVNs6', 1, 1, '03154223389', NULL, '2020-07-21 18:19:06', '2020-07-21 18:19:06'),
(14, 'dwalves', 'galilmolaie@gmail.com', NULL, '$2y$10$fh.FKIE5XYr5RtF4f3EHqup5kfuz4JZsyrqQxksBne/m47AtFHD6.', 1, 1, '03134223389', NULL, '2020-07-20 19:04:20', '2020-07-20 19:04:20'),
(13, 'dwalves', 'galilmoolaei@gmail.com', NULL, '$2y$10$H5ctTzlCXsNlZGYcWd35i.w0lgwI/qYSzz78zudyU69Nk1Vu3qiRW', 0, 1, '09878258389', NULL, '2020-07-18 07:33:23', '2020-07-19 21:57:31'),
(15, 'dwalves', 'galilmorfgolaei@gmail.com', NULL, '$2y$10$H5ctTzlCXsNlZGYcWd35i.w0lgwI/qYSzz78zudyU69Nk1Vu3qiRW', 0, 1, '09878058389', NULL, '2020-07-18 07:33:23', '2020-07-19 21:57:31');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
